#!/usr/bin/env python3

from sys import argv, stderr

from utils import extract_words


if len(argv) < 3:
    print("Usage: {} file... policy.py",
            file=stderr)
else:
    stock_files = argv[1:-1]
    policy_file = argv[-1]


stocks = []
for f in stock_files:
    with open(f) as fd:
        stocks += extract_words(fd)
# discard duplicates
stocks = set(stocks)

print(stocks)
