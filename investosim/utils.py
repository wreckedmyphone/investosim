"""Utility functions for Investosim."""


def extract_words(f):
    """Return the list of words in text file f."""

    words = []
    for line in f:
        words += line.split()

    return words
