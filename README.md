# Investosim - Stock market simulator

Investosim is a program that tests an investment policy/strategy on historical stock data. It tries to do it as mechanically as possible for it, resorting to randomness when required.

# How does Investosim work?

It is my belief that users will benefit from a broad overview of how the software actually works.

Investosim downloads stock data for a group of stocks provided by the user in the form of a text file. Data is retrived through the Alpha Vantage API. With the required data, Investosim is now ready to execute a given investment policy.

Investment policies must be created by users. See the documentation for more details on how to do this. Investment policies are subclasses of the InvestmentPolicy abstract class. The execute() method of the class is supplied with minute-wise stock data and it must decide what to do. At the end, the print() method is called which prints the results of the simulation. The topic is discussed in depth in the documentation.

# How to use Investosim?

Before you can start using Investosim, there are a couple of things you must prepare: the stocks to choose from, and the investment policy to follow.

To create a list of stocks for Investosim to choose from, simply create a file with the ticker for each stock on a separate line. You may name the file anything, and you may even create multiple files.

Creating an investment policy might be a bit complicated for users since it involves some programming. Please see the documentation.

Once you have the two things, you are ready to run the stock market simulation. Open a console (see the documentation for more details) and run the following command:

$ investosim (files with the tickers of stock) (python file with the code for the investment policy)

This may take several minutes. Investosim will run the stock market simulation, printing outcomes every year.
